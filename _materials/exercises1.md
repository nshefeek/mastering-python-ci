# Exercises part 1

All the exercises assume you're starting from folder `part1-middle`.

1. Add one more job that runs your favorite static analysis tool. If you don't have a favorite static analysis tool, use [ruff](https://docs.astral.sh/ruff/).
2. Add a plugin/configuration setting to display tests coverage.
3. (*) Replace all static analysis plugins in our `.gitlab-ci.yaml` with [pre-commit](https://pre-commit.com/) tool.
