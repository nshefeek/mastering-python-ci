# Mastering Python CI

Slides are available in a different branch (to keep the side of the `master` branch smaller for cloning): [slides part 1](https://gitlab.com/switowski/mastering-python-ci/-/blob/slides/_materials/slides-part1.pdf?ref_type=heads), [slides part 2](https://gitlab.com/switowski/mastering-python-ci/-/blob/slides/_materials/slides-part2.pdf?ref_type=heads).

When cloning this repository on slow internet, clone only the `master branch`:

```shell
git clone https://gitlab.com/switowski/mastering-python-ci.git --single-branch
```

## Installation

Unfortunately running CI pipelines locally is not as easy as following most of the other Python workshops where you can just download a Jupyter notebook file and have everything you need to follow the workshop. Hopefully we can all run our pipelines directly on GitLab.

To follow the exercises from this workshop you have the following options:

### (recommended) Push your code to GitLab

1. Clone this repository, so you have a copy in you GitLab account.
2. Follow the instructions from the trainer.
3. Push your changes to GitLab, so the GitLab CI will run your pipelines.

This is the easiest way to follow the workshop, but it depends on an external service (GitLab) being available and fast enough with your pipelines. The iteration cycle (time to change your code, push it, and see the results in GitLab CI) will be a bit slow (over 1 minute), so you might end up spending more time waiting for GitLab CI pipelines to finish running, than actually writing the code.

---

If GitLab CI is not available or you know how (and prefer) to run gitlab-runner locally on your computer, you can try one of the other methods below.

### `gitlab-runner exec shell`

This method will install `gitlab-runner` binary on your computer and use it to run CI jobs locally. The downside of this approach is that it's less secure (jobs will be running with permissions of the gitlab-runner's user, see more information [here](https://docs.gitlab.com/runner/security/index.html#usage-of-shell-executor)) and it will create additional folders and files (the runner will basically clone the repository to a subfolder and run jobs there).

To avoid the downsides of this method, you can use the next one that will run gitlab-runner in a Docker container. However running gitlab-runner with `shell` executor is **easier to set up and debug** in case of any problems.

1. Download this repository to your computer.
2. Follow the [Install GitLab Runner->Binaries](https://docs.gitlab.com/runner/install/#binaries) instructions to install a binary specific to your operating system, but **don't register it!**. You will be running gitlab-runner on your computer, so there is no need to register it or run it as a daemon mode. Just download the binary and make sure you can execute it by running `gitlab-runner --help` command.
3. Copy content of the exercises repository ("part1-middle" or "part2-middle") to the root folder of your project, do the exercise, push your code to the repository and run `gitlab-runner exec shell <name-of-the-job>` to execute a given job from the `.gitlab-ci.yml` file.

### `gitlab-runner exec docker`

Running gitlab-runner directly in shell is not the most secure thing to do (just like running *any* command downloaded from the Internet can pose a security risk), so instead you might consider running gitlab-runner in a Docker container. However running gitlab-runner with `shell` executor is easier to set up and debug in case of any problems.

If you don't want to run gitlab-runner in shell, you can try to run it in a docker container using `gitlab-runner exec docker` command. However, you will most likely need to adjust some settings, mount some volumes, etc. For example, on my computer, I could not get docker in docker to run until I used the following command (from [this StackOverflow answer](https://stackoverflow.com/a/65920577)):

```shell
docker run --entrypoint bash --rm -w $PWD -v $PWD:$PWD -v /var/run/docker.sock:/var/run/docker.sock gitlab/gitlab-runner:latest -c 'git config --global --add safe.directory "*";gitlab-runner exec docker test'
```

Looks complex? Yes. It took me many different tries to get this working. Now multiply this complexity by the number of people that are in this workshop and you will understand why I suggest to stick with running GitLab CI pipelines instead of spending 2 hours on making sure everyone can run gitlab-runner locally.

#### gitlab-runner `exec` caveats

The documentation for `gitlab-runner exec` says that it's deprecated, but for the time of the workshop it should work. You wouldn't anyway run the `gitlab-runner exec` locally in your daily work. We're only using it as a backup to GitLab CI pipelines.

Additionally, [not all the GitLab CI features are supported](https://docs.gitlab.com/runner/commands/index.html#limitations-of-gitlab-runner-exec) by the `gitlab-runner exec`. For example, secret variables, timeouts, triggers, etc. Most of those features don't matter during this workshop, unless you use pushing and pulling Docker images to GitLab container registry, as I show you in the 2nd part of the workshop - there we're using GitLab's environment variables that are not available locally.

#### Troubleshooting runners

In case of errors, first have a look if it's one mentioned in [GitLab's troubleshooting runner guide](https://docs.gitlab.com/runner/faq/)
