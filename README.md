# DICE ROLLER 3000 example project

Simple Python project that can be used as a base for building a continuous integration pipeline.

It consists of:

- main Python script `dice.py` that can be used to simulate rolling multiple dice
- `assets.py` which contains the ASCII art used to display dice in the terminal
- some tests in the `test_dice.py`

## Usage

```shell
$ python dice.py

# Enable PRO mode to use 9-sided dice (only available in the programming world!) instead of the free, 6-sided ones:
$ python dice.py --pro
```

Write a GitLab CI configuration that will run tests, format the code, and run it through your favorite static-analysis tool.
