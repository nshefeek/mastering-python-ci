from dice import single_roll, multiple_rolls


def test_single_die():
    "Rolling a single die should return results between 1 and 6."
    assert 1 <= single_roll() <= 6


def test_single_pro_die():
    "Rolling a single PRO die should return results between 1 and 9."
    assert 1 <= single_roll(pro=True) <= 9


def test_multiple_dice():
    "Rolling multiple dice should return multiple results between 1 and 6."
    results = multiple_rolls(1000)
    # Check that the face values are different.
    # This could in theory be a flaky test (if all the 1000 throws are the same) but the probability of this is so low,
    # that if you manage to fail this test, I recommend you play Eurojackpot with your luck.
    assert len(results) > 1
    assert set(results).issubset({1, 2, 3, 4, 5, 6})


def test_multiple_pro_dice():
    "Rolling multiple PRO dice should return multiple results between 1 and 9."
    results = multiple_rolls(1000, pro=True)
    # Check that the face values are different.
    # This could in theory be a flaky test (if all the 1000 throws are the same) but the probability of this is so low,
    # that if you manage to fail this test, I recommend you play Eurojackpot with your luck.
    assert len(results) > 1
    assert set(results).issubset({1, 2, 3, 4, 5, 6, 7, 8, 9}), "Invalid face values!"

    # Make sure that we actually have 7, 8 or 9 values in the results
    assert {7, 8, 9}.issubset(results), "We're not using PRO dice!"
